<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Incidents Controller
 *
 * @property \App\Model\Table\IncidentsTable $Incidents
 *
 * @method \App\Model\Entity\Incident[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class IncidentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Appusers']
        ];
        $incidents = $this->paginate($this->Incidents);

        $this->set(compact('incidents'));
    }

    /**
     * View method
     *
     * @param string|null $id Incident id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $incident = $this->Incidents->get($id, [
            'contain' => ['Appusers', 'Metadata']
        ]);

        $this->set('incident', $incident);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $incident = $this->Incidents->newEntity();
        if ($this->request->is('post')) {
            $incident = $this->Incidents->patchEntity($incident, $this->request->getData());
            if ($this->Incidents->save($incident)) {
                $this->Flash->success(__('The incident has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The incident could not be saved. Please, try again.'));
        }
        $appusers = $this->Incidents->Appusers->find('list', ['limit' => 200]);
        $this->set(compact('incident', 'appusers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Incident id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $incident = $this->Incidents->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $incident = $this->Incidents->patchEntity($incident, $this->request->getData());
            if ($this->Incidents->save($incident)) {
                $this->Flash->success(__('The incident has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The incident could not be saved. Please, try again.'));
        }
        $appusers = $this->Incidents->Appusers->find('list', ['limit' => 200]);
        $this->set(compact('incident', 'appusers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Incident id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $incident = $this->Incidents->get($id);
        if ($this->Incidents->delete($incident)) {
            $this->Flash->success(__('The incident has been deleted.'));
        } else {
            $this->Flash->error(__('The incident could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
