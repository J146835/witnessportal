<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Security;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     * @throws \Aura\Intl\Exception
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     * @throws \Aura\Intl\Exceptioncak
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @throws \Aura\Intl\Exception
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function forgotpassword(){
        if($this->request->is('post')){
            $myemail = $this->request->getData('email');
            $mytoken = Security::hash(Security::randomBytes(25));

            $userTable = TableRegistry::get('Users');
            $user = $userTable->find('all')->where(['email'=>$myemail])->first();
            $user->password = '';
            $user->token = $mytoken;
            if($userTable->save($user)){
                $this->Flash->success('Reset password link has been sent to your email ('.$myemail.'), please check your inbox.');

                Email::configTransport('mailtrap', [
                    'host' => 'smtp.mailtrap.io',
                    'port' => 2525,
                    'username' => 'b2042f5b441cec',
                    'password' => '02d1dc1045872a',
                    'className' => 'Smtp'
                ]);

                $email = new Email('default');
                $email->transport('mailtrap');
                $email->emailFormat('html');
                $email->template('default', 'default');
                $email->from('no-reply@witness.com', 'support');
                $email->subject('Please reset your password');
                $email->to($myemail);
                $email->send('Hi, '.$myemail.'<br/>Please click the link below to reset your password<br/><br/><a href="http://localhost:8765/users/resetpassword/'.$mytoken.'">Reset Password</a> ' );
            }
        }
    }

    public function resetpassword($token){
        if($this->request->is('post')){
            $hasher = new DefaultPasswordHasher();
            $mypass = $hasher->hash($this->request->getData('password'));

            $userTable = TableRegistry::get('Users');
            $user = $userTable->find('all')->where(['token'=>$token])->first();
            $user->password = $mypass;
            if($userTable->save($user) AND !$this->request->session()->read('Auth.User.id')){
                return $this->redirect(['action'=>'login']);
            } else if($this->request->session()->read('Auth.User.id')){
                return $this->redirect('/users/view/'.$this->request->session()->read('Auth.User.id'));
            } else {
                $this->Flash->error('Something went wrong');
            }
        }
    }

    public function login(){
        if($this->request->is('post')){
            $user = $this->Auth->identify();
            if($user){
                $this->Auth->setUser($user);
                //return $this->redirect($this->Auth->redirectUrl());
                return $this->redirect('/users/view/'.$this->request->session()->read('Auth.User.id'));
            } else{
                $this->Flash->error('Your username or password is incorrect');
            }
        }
    }

    public function logout(){
        return $this->redirect($this->Auth->logout());
    }

    public function register(){
        if($this->request->is('post')){
            $userTable = TableRegistry::get('Users');
            $user = $userTable->newEntity();

            $hasher = new DefaultPasswordHasher();
            $myname = $this->request->getData('name');
            $myemail = $this->request->getData('email');
            //$mypass = Security::hash($this->request->getData('password'), 'sha256', false);
            $mypass = $this->request->getData('password'); // will convert to bcrypt hash password
            $mytoken = Security::hash(Security::randomBytes(32));
            $myrole = $this->request->data['role'] = 'USER';

            $user->name = $myname;
            $user->email = $myemail;
            $user->password = $hasher->hash($mypass);
            $user->role = $myrole;
            $user->token = $mytoken;
            $user->created_at = date('Y-m-d H:i:s');
            $user->updated_at = date('Y-m-d H:i:s');
            if($userTable->save($user)){
                $this->Flash->set('Register successful, your confirmation email has been sent', ['element'=>'success']);

                Email::configTransport('mailtrap', [
                    'host' => 'smtp.mailtrap.io',
                    'port' => 2525,
                    'username' => 'b2042f5b441cec',
                    'password' => '02d1dc1045872a',
                    'className' => 'Smtp'
                ]);

                $email = new Email('default');
                $email->transport('mailtrap');
                $email->emailFormat('html');
                $email->from('no-reply@witness.com', 'support');
                $email->subject('please confirm your email to activate your account');
                $email->to($myemail);
                $email->send('Hi, '.$myname.'<br/>Please confirm your email, link below:<br/><a href="http://localhost:8765/users/verification/'.$mytoken.'">Verification Email</a><br/>Thank you for joining us');

            } else{
                $this->Flash->set('Register failed, ', ['element'=>'error']);
            }

        }
    }

    public function verification($token){
        $userTable = TableRegistry::get('users');
        $verify = $userTable->find('all')->where(['token'=>$token])->first();
        $verify->verified = '1';
        $userTable->save($verify);
    }
}
