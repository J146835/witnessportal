<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Appusers Controller
 *
 * @property \App\Model\Table\AppusersTable $Appusers
 *
 * @method \App\Model\Entity\Appuser[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AppusersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $appusers = $this->paginate($this->Appusers);

        $this->set(compact('appusers'));
    }

    /**
     * View method
     *
     * @param string|null $id Appuser id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $appuser = $this->Appusers->get($id, [
            'contain' => ['Incidents']
        ]);

        $this->set('appuser', $appuser);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $appuser = $this->Appusers->newEntity();
        if ($this->request->is('post')) {
            $appuser = $this->Appusers->patchEntity($appuser, $this->request->getData());
            if ($this->Appusers->save($appuser)) {
                $this->Flash->success(__('The appuser has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The appuser could not be saved. Please, try again.'));
        }
        $this->set(compact('appuser'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Appuser id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $appuser = $this->Appusers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $appuser = $this->Appusers->patchEntity($appuser, $this->request->getData());
            if ($this->Appusers->save($appuser)) {
                $this->Flash->success(__('The appuser has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The appuser could not be saved. Please, try again.'));
        }
        $this->set(compact('appuser'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Appuser id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $appuser = $this->Appusers->get($id);
        if ($this->Appusers->delete($appuser)) {
            $this->Flash->success(__('The appuser has been deleted.'));
        } else {
            $this->Flash->error(__('The appuser could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
