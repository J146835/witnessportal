 <?php if($this->request->session()->read('Auth.User.id')) { ?>
    <nav class="cell medium-2" id="actions-sidebar">
        <ul class="side-nav">
            <li class="heading"><?= __('Actions') ?></li>
            <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('New Incident'), ['controller' => 'Incidents', 'action' => 'add']) ?></li>
        </ul>
    </nav>
    <div class="container cell medium-10">
        <?= $this->fetch('content') ?>
    </div>
<?php } ?>