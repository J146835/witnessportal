<?= $this->Flash->render() ?>
<?php if(!$this->request->session()->read('Auth.User.id')) { ?>
    <div class="card">
        <h3 class="card-divider">Register</h3>
        <div class="card-section">
            <?php echo $this->Form->create()?>
            <div class="form-group">
                <?php echo $this->Form->input('name', ['class'=>'form-control', 'required']) ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('email', ['class'=>'form-control', 'required']) ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('password', ['class'=>'form-control', 'required']) ?>
            </div>
            <?php
            echo $this->Form->button('Register', ['class'=>'button']);
            echo $this->Form->end();
            ?>
        </div>
    </div>
<?php } else { ?>
    <p>You are already logged in</p>
<?php } ?>





