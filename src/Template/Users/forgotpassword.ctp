<?php if(!$this->request->session()->read('Auth.User.id')) { ?>
    <?php echo $this->Flash->render() ?>
    <div class="card">
        <h3 class="card-divider">Forgot Password</h3>
        <div class="card-section">
            <?php echo $this->Form->create() ?>
            <div class="form-group">
                <?php echo $this->Form->input('email', ['class'=>'form-control']) ?>
            </div>
            <?php
            echo $this->Form->button('Get New Password', ['class'=>'button']);
            echo $this->Form->end();
            ?>
        </div>
    </div>
<?php } else { ?>
    <p>You are already logged in</p>
<?php } ?>


