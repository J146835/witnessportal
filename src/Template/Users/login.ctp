<?php if(!$this->request->session()->read('Auth.User.id')) { ?>
<?php echo $this->Flash->render()?>
    <div class="card login">
        <h3 class="card-divider">Login</h3>
        <div class="card-section">
            <?php echo $this->Form->create(); ?>
            <div class="form-group">
                <?php echo $this->Form->input('email', ['class'=>'form-control']) ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('password', ['class'=>'form-control']) ?>
            </div>
            <?php
            echo $this->Form->button('Login', ['class'=>'button']);
            //echo $this->Html->link('Register', ['action'=>'register'], ['class'=>'button']);
            //echo $this->Html->link('Forgot Password', ['action'=>'forgotpassword'], ['class'=>'button secondary']);
            echo $this->Form->end();
            ?>
        </div>
    </div>
<?php } else { ?>
    <p>You are already Logged in</p>
<?php } ?>



