<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="users add medium-10 cell content">
    <?php if($this->request->session()->read('Auth.User.role') == "ADMIN") { ?>
        <?= $this->Form->create($user) ?>
        <fieldset>
            <legend><?= __('Add User') ?></legend>
            <?php
            echo $this->Form->control('name');
            echo $this->Form->control('email');
            echo $this->Form->control('password');
            echo $this->Form->control('role');
            echo $this->Form->control('verified');
            echo $this->Form->control('token');
            ?>
        </fieldset>
        <?= $this->Form->button(__('Submit')) ?>
        <?= $this->Form->end() ?>
    <?php } else { ?>
        <p>You are not authorized to view this page</p>
    <?php } ?>
</div>


