 <?php echo $this->Flash->render() ?>
<div class="card">
    <?php if(!$this->request->session()->read('Auth.User.id')) { ?>
        <h3 class="card-divider">Reset Password</h3>
    <?php } else { ?>
        <h3 class="card-divider">Change Password</h3>
    <?php } ?>
    <div class="card-section">
        <?php echo $this->Form->create() ?>
        <div class="form-group">
            <?php echo $this->Form->input('password', ['password'=>'form-control']) ?>
        </div>
        <?php
        echo $this->Form->button('Reset Password', ['class'=>'button']);
        echo $this->Form->end();
        ?>
    </div>
</div>
