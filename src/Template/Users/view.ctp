<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="users view medium-10 cell content">
    <?php if($this->request->session()->read('Auth.User.role') == "ADMIN" OR $this->request->session()->read('Auth.User.id') == $user->id) { ?>
        <h3><?= h($user->name) ?></h3>
        <table class="vertical-table">
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($user->name) ?></td>
            </tr>

            <?php if($this->request->session()->read('Auth.User.role') == "ADMIN") { ?>
                <tr>
                    <th scope="row"><?= __('Email') ?></th>
                    <td><?= h($user->email) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Password') ?></th>
                    <td><?= h($user->password) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Role') ?></th>
                    <td><?= h($user->role) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Token') ?></th>
                    <td><?= h($user->token) ?></td>
                </tr>
            <?php } ?>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($user->id) ?></td>
            </tr>

            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($user->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($user->modified) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Verified') ?></th>
                <td><?= $user->verified ? __('Yes') : __('No'); ?></td>
            </tr>
        </table>
        <?php echo '<a href="http://localhost:8765/users/resetpassword/'.$user->token.'" class="button">Change Password</a>'?>
        <div class="related">

        </div>
    <!--ends the first if statement-->
    <?php } else { ?>
        <p>You are not authorized to view this page</p>
    <?php } ?>
</div>