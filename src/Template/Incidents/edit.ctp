<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Incident $incident
 */
?>

<div class="incidents form medium-10 cell content">
    <?php if($this->request->session()->read('Auth.User.role') == "ADMIN" OR $this->request->session()->read('Auth.User.id') == $incident->user_id) { ?>
        <?= $this->Form->create($incident) ?>
        <fieldset>
            <h4><?= __('Edit Incident') ?></h4>
            <?php
            echo $this->Form->control('description');
            //echo $this->Form->control('latitude');
            //echo $this->Form->control('longitude');
            //echo $this->Form->control('user_id', ['options' => $users]);
            ?>
        </fieldset>
        <?= $this->Form->button(__('Submit'), ['class'=>'button']) ?>
        <?= $this->Form->end() ?>
    <?php } else { ?>
        <p>You are not authorized to view this page</p>
    <?php } ?>
</div>

