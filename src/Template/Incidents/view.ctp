<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Incident $incident
 */
?>
<div class="incidents view medium-10 cell content">
    <?php if($this->request->session()->read('Auth.User.role') == "ADMIN" OR $this->request->session()->read('Auth.User.id') == $incident->user_id) { ?>
        <h3>Incident Details ID:<?= h($incident->id) ?></h3>
        <table class="vertical-table">
            <tr>
                <th scope="row"><?= __('Description') ?></th>
                <td><?= h($incident->description) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('User') ?></th>
                <td><?= $incident->has('user') ? $this->Html->link($incident->user->name, ['controller' => 'Users', 'action' => 'view', $incident->user->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($incident->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Latitude') ?></th>
                <td><?= $this->Number->format($incident->latitude) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Longitude') ?></th>
                <td><?= $this->Number->format($incident->longitude) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($incident->created) ?></td>
            </tr>
        </table>
        <?php echo $this->Html->link('Add Image', ['controller'=>'Images', 'action'=>'add'], ['class'=>'button'], ['id'=>$incident->id]); ?>
        <?php echo $this->Html->link('Notify User', ['controller'=>'Users', 'action'=>'notifyuser'], ['class'=>'button'], ['incident_id'=>$incident->id]); ?>
        <div id="googleMap" style="width:100%;height:400px;" class="medium-10 cell" onload="ShowMap()"></div>
        <?php } else { ?>
            <p>You are not authorized to view this page</p>
        <?php } ?>
    </div>

    <script>
        // shows the incidents location
        function ShowMap() {

            var lat = <?php echo $incident->latitude ?>;
            var lng = <?php echo $incident->longitude ?>;

            var myCenter = new google.maps.LatLng(lat, lng);
            var marker;

            function initialize() {
                var mapProp = {
                    center: myCenter,
                    zoom: 15,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

                var marker = new google.maps.Marker({
                    position: myCenter,
                });

                marker.setMap(map);
            }

            initialize();
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-dTokUKaCpSpuPq3sc3V1_LJ2Aq7p_7I&callback=ShowMap"></script>
</div>

