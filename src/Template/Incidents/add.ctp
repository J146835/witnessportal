<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Incident $incident
 */
?>
<div class="incidents form content">
    <?= $this->Form->create($incident) ?>
    <fieldset>
        <legend><?= __('Add Incident') ?></legend>
        <?php
        echo $this->Form->control('description');
        echo $this->Form->control('latitude');
        echo $this->Form->control('longitude');
        //echo $this->Form->control('user_id', ['options' => $users]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'button']) ?>
    <?= $this->Form->end() ?>

    <div id="googleMap" style="width:100%;height:400px;" onload="ShowMap()"></div>
    <script>
        var addmap, infoWindow;
        // Gets users location
        function InitMap() {
            addmap = new google.maps.Map(document.getElementById('googleMap'), {
                center: {lat: -34.397, lng: 150.644},
                zoom: 6
            });
            infoWindow = new google.maps.InfoWindow;

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    var marker = new google.maps.Marker({
                        position: pos,
                    });

                    /*infoWindow.setPosition(pos);
                    infoWindow.setContent('Location found.');
                    infoWindow.open(addmap);*/
                    addmap.setCenter(pos);
                    marker.setMap(addmap);

                    document.getElementById("latitude").value = pos.lat;
                    document.getElementById("longitude").value = pos.lng;


                }, function() {
                    handleLocationError(true, infoWindow, addmap.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, addmap.getCenter());
            }
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(addmap);
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-dTokUKaCpSpuPq3sc3V1_LJ2Aq7p_7I&callback=InitMap"></script>
</div>




