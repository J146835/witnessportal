<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Incident[]|\Cake\Collection\CollectionInterface $incidents
 */
?>
<div class="incidents index medium-10 cell content">
    <h3><?= __('Incidents') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('description') ?></th>
            <th scope="col"><?= $this->Paginator->sort('latitude') ?></th>
            <th scope="col"><?= $this->Paginator->sort('longitude') ?></th>
            <th scope="col"><?= $this->Paginator->sort('created') ?></th>
            <?php if($this->request->session()->read('Auth.User.role') == "ADMIN"){ ?>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
            <?php } ?>
            <th scope="col" class="actions"><?= __('Actions') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php if($this->request->session()->read('Auth.User.role') == "ADMIN") { ?>
            <?php foreach ($incidents as $incident): ?>
                <tr>
                    <td><?= $this->Number->format($incident->id) ?></td>
                    <td><?= h($incident->description) ?></td>
                    <td><?= $this->Number->format($incident->latitude) ?></td>
                    <td><?= $this->Number->format($incident->longitude) ?></td>
                    <td><?= h($incident->created) ?></td>
                    <td><?= $incident->has('user') ? $this->Html->link($incident->user->name, ['controller' => 'Users', 'action' => 'view', $incident->user->id]) : '' ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $incident->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $incident->id]) ?>
                       <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $incident->id], ['confirm' => __('Are you sure you want to delete # {0}?', $incident->id)]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php } else if($this->request->session()->read('Auth.User.role') == "USER") { ?>
            <?php foreach ($incidents as $incident): ?>
                    <tr>
                        <td><?= $this->Number->format($incident->id) ?></td>
                        <td><?= h($incident->description) ?></td>
                        <td><?= $this->Number->format($incident->latitude) ?></td>
                        <td><?= $this->Number->format($incident->longitude) ?></td>
                        <td><?= h($incident->created) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $incident->id]); ?>
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $incident->id]); ?>
                        </td>
                    </tr>
            <?php endforeach; ?>
        <?php } ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
