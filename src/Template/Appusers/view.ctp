<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Appuser $appuser
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Appuser'), ['action' => 'edit', $appuser->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Appuser'), ['action' => 'delete', $appuser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $appuser->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Appusers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Appuser'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Incidents'), ['controller' => 'Incidents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Incident'), ['controller' => 'Incidents', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="appusers view large-9 medium-8 columns content">
    <h3><?= h($appuser->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($appuser->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Token') ?></th>
            <td><?= h($appuser->token) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($appuser->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Incidents') ?></h4>
        <?php if (!empty($appuser->incidents)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Latitude') ?></th>
                <th scope="col"><?= __('Longitude') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Appuser Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($appuser->incidents as $incidents): ?>
            <tr>
                <td><?= h($incidents->id) ?></td>
                <td><?= h($incidents->description) ?></td>
                <td><?= h($incidents->latitude) ?></td>
                <td><?= h($incidents->longitude) ?></td>
                <td><?= h($incidents->created) ?></td>
                <td><?= h($incidents->appuser_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Incidents', 'action' => 'view', $incidents->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Incidents', 'action' => 'edit', $incidents->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Incidents', 'action' => 'delete', $incidents->id], ['confirm' => __('Are you sure you want to delete # {0}?', $incidents->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
