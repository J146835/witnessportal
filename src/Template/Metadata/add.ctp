<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Metadata $metadata
 */
?>
<div class="metadata form large-9 medium-8 columns content">
    <?= $this->Form->create($metadata) ?>
    <fieldset>
        <legend><?= __('Add Metadata') ?></legend>
        <?php
            echo $this->Form->control('fileName');
            echo $this->Form->control('fileSize');
            echo $this->Form->control('fileDateTime');
            echo $this->Form->control('incident_id', ['options' => $incidents]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
