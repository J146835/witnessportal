<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Metadata $metadata
 */
?>
<div class="metadata view large-9 medium-8 columns content">
    <h3><?= h($metadata->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('FileName') ?></th>
            <td><?= h($metadata->fileName) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('FileSize') ?></th>
            <td><?= h($metadata->fileSize) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('FileDateTime') ?></th>
            <td><?= h($metadata->fileDateTime) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Incident') ?></th>
            <td><?= $metadata->has('incident') ? $this->Html->link($metadata->incident->id, ['controller' => 'Incidents', 'action' => 'view', $metadata->incident->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($metadata->id) ?></td>
        </tr>
    </table>
</div>
