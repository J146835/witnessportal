<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Metadata[]|\Cake\Collection\CollectionInterface $metadata
 */
?>
<div class="metadata index large-9 medium-8 columns content">
    <h3><?= __('Metadata') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fileName') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fileSize') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fileDateTime') ?></th>
                <th scope="col"><?= $this->Paginator->sort('incident_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($metadata as $metadata): ?>
            <tr>
                <td><?= $this->Number->format($metadata->id) ?></td>
                <td><?= h($metadata->fileName) ?></td>
                <td><?= h($metadata->fileSize) ?></td>
                <td><?= h($metadata->fileDateTime) ?></td>
                <td><?= $metadata->has('incident') ? $this->Html->link($metadata->incident->id, ['controller' => 'Incidents', 'action' => 'view', $metadata->incident->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $metadata->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $metadata->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $metadata->id], ['confirm' => __('Are you sure you want to delete # {0}?', $metadata->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
