<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <!-- not sure if these are all required or correctly configured-->
    <?= $this->Html->css('foundation.min.css') ?>
    <?= $this->Html->css('font-awesome.min.css') ?>
    <!-- custom styles from app.scss-->
    <?= $this->Html->css('app.css') ?>

    <?= $this->Html->script('/vendor/jquery.min.js') ?>
    <?= $this->Html->script('/vendor/what-input.min.js') ?>
    <!-- popper may not be used -->
    <?= $this->Html->script('popper.min.js') ?>
    <?= $this->Html->script('/vendor/foundation.min.js') ?>


    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <div class="wrapper">
        <header>
            <nav class="top-bar expanded" data-topbar role="navigation">
                <ul class="title-area large-3 medium-4 cells">
                    <!-- Logo could replace the contents of name-->
                    <li class="name">
                        <h1><?= "Witness"//$this->fetch('title') ?></h1>
                    </li>
                </ul>
                <div class="top-bar-section">
                    <ul class="right menu horizontal">

                        <?php if($this->request->session()->read('Auth.User.id')) { ?>

                            <li><a href="/users/view/<?= $this->request->session()->read('Auth.User.id') ?>"><?= $this->request->session()->read('Auth.User.name') ?></a></li>
                            <li><?php echo $this->Html->link('logout',['controller'=>'users', 'action'=>'logout']) ?></li>

                        <?php }else{ ?>

                            <?php if ($this->request->here != '/users/login') { ?>
                                <li><?= $this->Html->link('Login', ['controller'=>'users', 'action'=>'login']);?></li>
                            <?php } ?>

                            <?php if ($this->request->here != '/users/register') { ?>
                                <li><?= $this->Html->link('Register', ['controller'=>'users', 'action'=>'register']);?></li>
                            <?php } ?>

                            <li><?= $this->Html->link('Forgot Password?', ['controller'=>'users', 'action'=>'forgotpassword']);?></li>

                        <?php } ?>
                    </ul>
                </div>
            </nav>
        </header>
        <?= $this->Flash->render() ?>

        <div class="grid-x">
            <?php if($this->request->session()->read('Auth.User.role') == "ADMIN"){

                echo $this->element('sidebar/sidebar_admin', ['viewName'=>$this->name]);
            }else if($this->request->session()->read('Auth.User.role') == "USER"){
                echo $this->element('sidebar/sidebar_user', ['viewName'=> $this->name]);
            } else { ?>
                <div class="container cell medium-4 medium-offset-4">
                    <?= $this->fetch('content') ?>
                </div>
            <?php } ?>
        </div>

    </div>

        <footer>
            <div class="grid-x test">
                <div class="cell medium-2 footer-list">
                    <ul class="menu horizontal links">
                        <li><?= $this->Html->link('About Us', ['controller'=>'pages', 'action'=>'display', 'about']);?></li>
                        <li><?= $this->Html->link('Contact Us', ['controller'=>'pages', 'action'=>'display', 'contact']);?></li>
                    </ul>
                </div>
                <div class="cell medium-7"></div>
                <div class="cell medium-3 copyright">
                    <p>Witness Project &copy; 2018</p>
                </div>
            </div>
        </footer>
    </body>
</html>
