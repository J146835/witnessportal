<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Metadata Entity
 *
 * @property int $id
 * @property string $fileName
 * @property string $fileSize
 * @property string $fileDateTime
 * @property int $incident_id
 *
 * @property \App\Model\Entity\Incident $incident
 */
class Metadata extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fileName' => true,
        'fileSize' => true,
        'fileDateTime' => true,
        'incident_id' => true,
        'incident' => true
    ];
}
