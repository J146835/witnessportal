<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Incident Entity
 *
 * @property int $id
 * @property string $description
 * @property float $latitude
 * @property float $longitude
 * @property \Cake\I18n\FrozenTime $created
 * @property int $appuser_id
 *
 * @property \App\Model\Entity\Appuser $appuser
 * @property \App\Model\Entity\Metadata[] $metadata
 */
class Incident extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'description' => true,
        'latitude' => true,
        'longitude' => true,
        'created' => true,
        'appuser_id' => true,
        'appuser' => true,
        'metadata' => true
    ];
}
