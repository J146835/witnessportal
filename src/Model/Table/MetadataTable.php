<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Metadata Model
 *
 * @property \App\Model\Table\IncidentsTable|\Cake\ORM\Association\BelongsTo $Incidents
 *
 * @method \App\Model\Entity\Metadata get($primaryKey, $options = [])
 * @method \App\Model\Entity\Metadata newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Metadata[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Metadata|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Metadata|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Metadata patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Metadata[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Metadata findOrCreate($search, callable $callback = null, $options = [])
 */
class MetadataTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('metadata');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Incidents', [
            'foreignKey' => 'incident_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('fileName')
            ->maxLength('fileName', 255)
            ->allowEmpty('fileName');

        $validator
            ->scalar('fileSize')
            ->maxLength('fileSize', 255)
            ->allowEmpty('fileSize');

        $validator
            ->scalar('fileDateTime')
            ->maxLength('fileDateTime', 255)
            ->allowEmpty('fileDateTime');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['incident_id'], 'Incidents'));

        return $rules;
    }
}
