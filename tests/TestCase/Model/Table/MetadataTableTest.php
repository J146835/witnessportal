<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MetadataTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MetadataTable Test Case
 */
class MetadataTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MetadataTable
     */
    public $Metadata;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.metadata',
        'app.incidents'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Metadata') ? [] : ['className' => MetadataTable::class];
        $this->Metadata = TableRegistry::getTableLocator()->get('Metadata', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Metadata);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
